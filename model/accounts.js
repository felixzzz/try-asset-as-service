"use strict";

module.exports = function(sequelize, DataTypes) {
    var Account = sequelize.define('accounts', {
        id: {
            type: DataTypes.UUID,
            defaultValue: DataTypes.UUIDV4,
            primaryKey: true
        },
        first_name: {type:DataTypes.STRING(100)},
        last_name: {type:DataTypes.STRING(100)},
        email: {type:DataTypes.STRING(100)},
        mobile: {type:DataTypes.STRING(50)},
        private_seed: {type:DataTypes.STRING},
        encrypted_private_seed: {type:DataTypes.STRING},
        api_client_id:{type:DataTypes.CHAR(32)},
        created_at:{type:DataTypes.DATE},
        updated_at:{type:DataTypes.DATE},
    },{underscore:true});
    return Account;
};

