// This code is built by Sequelize No Changes Required. Only Update Database Details
var fs        = require('fs')
    , path      = require('path')
    , Sequelize = require('sequelize')
    , lodash    = require('lodash')
    , sequelize = new Sequelize('fundplaces_node', 'root', '', {
            host: 'localhost',
            dialect: 'mysql',
            pool: {
                max: 5,
                min: 0,
                idle: 10000
            }
        })
    , db        = {};

fs
    .readdirSync(__dirname)
    .filter(function(file) {
        return ((file.indexOf('.') !== 0) && (file !== 'index.js') && (file.slice(-3) === '.js'));
    })
    .forEach(function(file) {
        var model = sequelize.import(path.join(__dirname, file));
        db[model.name] = model;
    });

Object.keys(db).forEach(function(modelName) {
    if (db[modelName].options.hasOwnProperty('associate')) {
        db[modelName].options.associate(db);
    }
});

// describe relationships
//(function(m) {
    //m.PhoneNumber.belongsTo(m.User);
    //m.Task.belongsTo(m.User);
    //m.User.hasMany(m.Task);
    //m.User.hasMany(m.PhoneNumber);
//})(module.exports);


module.exports = lodash.extend({
    sequelize: sequelize,
    Sequelize: Sequelize
}, db);
