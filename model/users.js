"use strict";

module.exports = function(sequelize, DataTypes) {
    var Users = sequelize.define('users', {
        uuid: {
            type: DataTypes.UUID,
            defaultValue: DataTypes.UUIDV4,
            primaryKey: true
        },
        email: DataTypes.STRING,
        name: DataTypes.STRING,
    });

    return Users;
};

