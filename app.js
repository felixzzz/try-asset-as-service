//import restify from 'restify'
var restify = require('restify');
var db = require('./model');

var server = restify.createServer({
    name: 'FPBlockChain',
    version: '1.0.0'
});

var PATH = '/jobs';

server.use(restify.acceptParser(server.acceptable));
server.use(restify.queryParser());
server.use(restify.bodyParser());

server.get('/echo/:name', function (req, res, next) {


    db.sequelize.sync({force:true}).then(function() {
        return db.users.create({
            email: req.params.name+'@email.com',
            name: req.params.name
        });
    }).then(function(user) {
        console.log(user.get({
            plain: true
        }));
    });
    res.send(req.params);
    return next();
});
var ip_addr = '127.0.0.1';
var port    =  '8080';
server.listen(port ,ip_addr, function(){
    console.log('%s listening at %s', server.name, server.url);
});