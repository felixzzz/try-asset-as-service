'use strict';

module.exports = {
  up: function (queryInterface, DataTypes) {
    /*
      Add altering commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.createTable('users', { id: Sequelize.INTEGER });
    */
    queryInterface.createTable(
        'api_clients',
        {
            id: {
                type: DataTypes.UUID,
                defaultValue: DataTypes.UUIDV4,
                primaryKey: true
            },
            name: DataTypes.STRING,
            public_key: DataTypes.STRING,
            private_key: DataTypes.STRING,
            key_at: DataTypes.DATE,
            role: DataTypes.STRING(20),
            account_permission	:DataTypes.BOOLEAN,
            coin_permission	:DataTypes.BOOLEAN,
            token_permission	:DataTypes.BOOLEAN,
            created_at:DataTypes.DATE,
            updated_at:DataTypes.DATE
        }
    );
    queryInterface.createTable(
        'accounts',
        {
            id: {
                type: DataTypes.UUID,
                defaultValue: DataTypes.UUIDV4,
                primaryKey: true
            },
            first_name: DataTypes.STRING(100),
            last_name: DataTypes.STRING(100),
            email: DataTypes.STRING(100),
            mobile: DataTypes.STRING(50),
            private_seed: DataTypes.STRING,
            encrypted_private_seed: DataTypes.STRING,
            api_client_id:DataTypes.CHAR(32),
            created_at:DataTypes.DATE,
            updated_at:DataTypes.DATE,
        }
    );
    queryInterface.createTable(
        'balances',
        {
            id: {
                type: DataTypes.UUID,
                defaultValue: DataTypes.UUIDV4,
                primaryKey: true
            },
            account_id:DataTypes.CHAR(32),
            coin_id:DataTypes.CHAR(32),
            first_name: DataTypes.STRING(100),
            balance_amt:DataTypes.BIGINT,
            available_balance_amt:DataTypes.BIGINT,
            locked_balance_amt:DataTypes.BIGINT,
            created_at:DataTypes.DATE,
            updated_at:DataTypes.DATE,
        }
    );
    queryInterface.createTable(
        'coins',
        {
            id: {
                type: DataTypes.UUID,
                defaultValue: DataTypes.UUIDV4,
                primaryKey: true
            },
            name	: DataTypes.STRING,
            description	: DataTypes.TEXT,
            block_chain_asset_id	: DataTypes.STRING(100),
            api_client_id	: DataTypes.CHAR(36),
            asset_url	: DataTypes.STRING(255),
            coin_type	: DataTypes.STRING(20),
            purchase_coin_id	: DataTypes.CHAR(36),
            created_at:DataTypes.DATE,
            updated_at:DataTypes.DATE,
        }
    );
    queryInterface.createTable(
        'coin_details',
        {
            id: {
                type: DataTypes.UUID,
                defaultValue: DataTypes.UUIDV4,
                primaryKey: true
            },
            coin_id:DataTypes.CHAR(36),
            issued_amt:DataTypes.BIGINT,
            circulation_amt:DataTypes.BIGINT,
            nav_amt:DataTypes.BIGINT,
            nav_at:DataTypes.DATE,
            created_at:DataTypes.DATE,
            updated_at:DataTypes.DATE,
        }
    );
    queryInterface.createTable(
        'coin_prices',
        {
            id: {
                type: DataTypes.UUID,
                defaultValue: DataTypes.UUIDV4,
                primaryKey: true
            },
            coin_id:DataTypes.CHAR(36),
            price_in_tokens:DataTypes.BIGINT,
            price_at:DataTypes.DATEONLY,
            created_at:DataTypes.DATE,
        }
    );
    queryInterface.createTable(
        'trading_orders',
        {
            id: {
                type: DataTypes.UUID,
                defaultValue: DataTypes.UUIDV4,
                primaryKey: true
            },
            account_id:DataTypes.CHAR(36),
            coin_id:DataTypes.CHAR(36),
            order_type:DataTypes.STRING(20),
            all_or_nothing:DataTypes.BOOLEAN,
            buy:DataTypes.BOOLEAN,
            coin_amt:DataTypes.BIGINT,
            price_in_tokens:DataTypes.BIGINT,
            submit_at:DataTypes.DATE,
            expire_at:DataTypes.DATEONLY,
            status:DataTypes.STRING(20),
            created_at:DataTypes.DATE,
            updated_at:DataTypes.DATE,
        }
    );
    queryInterface.createTable(
        'transactions',
        {
            id: {
                type: DataTypes.UUID,
                defaultValue: DataTypes.UUIDV4,
                primaryKey: true
            },
            from_account_id:DataTypes.CHAR(36),
            to_account_id:DataTypes.CHAR(36),
            blockchain_transaction_id:DataTypes.STRING(100),
            trading_order_id:DataTypes.CHAR(36),
            coin_id:DataTypes.CHAR(36),
            coin_amt:DataTypes.BIGINT,
            transaction_start_at:DataTypes.DATE,
            transaction_confirm_at:DataTypes.DATE,
            status:DataTypes.STRING(20),
            created_at:DataTypes.DATE,
            updated_at:DataTypes.DATE,
        }
    );
  },

  down: function (queryInterface, Sequelize) {
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.dropTable('users');
    */
    queryInterface.dropTable('api_clients'),
    queryInterface.dropTable('api_clients'),
    queryInterface.dropTable('accounts'),
    queryInterface.dropTable('balances'),
    queryInterface.dropTable('coins'),
    queryInterface.dropTable('coin_details'),
    queryInterface.dropTable('coin_prices'),
    queryInterface.dropTable('trading_orders'),
    queryInterface.dropTable('transactions')
  }
};
